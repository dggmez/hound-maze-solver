﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HoundMazeSolver
{
    /// <summary>
    /// This is a simple graph data structure implementation using adjacency lists.
    /// It is specifically implemented to solve the challenge so it follows a
    /// non-general approach to update the adjacency lists for the vertexes.
    /// </summary>
    class Graph
    {
        private Dictionary<Point, List<Point>> adjacencyLists;

        /// <summary>
        /// Returns a list containing all the vertexes in the graph.
        /// </summary>
        public List<Point> Vertexes
        {
            get
            {
                return adjacencyLists.Keys.ToList();
            }
        }

        /// <summary>
        /// Initializes an instance of the class.
        /// </summary>
        public Graph()
        {
            adjacencyLists = new Dictionary<Point, List<Point>>();
        }

        /// <summary>
        /// Adds a vertex to the graph and updates the adjacency lists of the vertex
        /// and the neighbor vertexes.
        /// </summary>
        /// <param name="point"></param>
        public void AddVertex(Point point)
        {
            adjacencyLists.Add(point, new List<Point>());
            UpdateAdjacencyLists(point);
        }

        private void UpdateAdjacencyLists(Point point)
        {
            AddAdjacentVertex(point, new Point(point.X - 1, point.Y));
            AddAdjacentVertex(point, new Point(point.X + 1, point.Y));
            AddAdjacentVertex(point, new Point(point.X, point.Y - 1));
            AddAdjacentVertex(point, new Point(point.X, point.Y + 1));
        }

        private void AddAdjacentVertex(Point point, Point adjacentPoint)
        {
            if (adjacencyLists.ContainsKey(adjacentPoint))
            {
                if (!adjacencyLists[point].Contains(adjacentPoint))
                {
                    adjacencyLists[point].Add(adjacentPoint);
                }
                if (!adjacencyLists[adjacentPoint].Contains(point))
                {
                    adjacencyLists[adjacentPoint].Add(point);
                }
            }
        }

        /// <summary>
        /// Returns the shortest path between the two vertexes using a
        /// breadth-first search approach.
        /// </summary>
        /// <param name="startpoint">The initial point.</param>
        /// <param name="endpoint">The target point.</param>
        /// <returns></returns>
        public List<Point> FindPath(Point startpoint, Point endpoint)
        {
            const string exceptionMessage = "The point is not located in a walkable cell of the map.";

            if (!adjacencyLists.ContainsKey(startpoint))
            {
                throw new ArgumentException(exceptionMessage, nameof(startpoint));
            }

            if (!adjacencyLists.ContainsKey(endpoint))
            {
                throw new ArgumentException(exceptionMessage, nameof(endpoint));
            }

            if (startpoint == endpoint)
            {
                return new List<Point> { startpoint };
            }
            HashSet<Point> seen = new HashSet<Point>();
            Dictionary<Point, Point> previousPoint = new Dictionary<Point, Point>();
            Queue<Point> queue = new Queue<Point>();
            queue.Enqueue(startpoint);
            seen.Add(startpoint);
            while (queue.Count != 0)
            {
                Point p = queue.Dequeue();
                if (p == endpoint)
                {
                    break;
                }
                foreach (var adjacentVertex in adjacencyLists[p])
                {
                    if (!seen.Contains(adjacentVertex))
                    {
                        queue.Enqueue(adjacentVertex);
                        seen.Add(adjacentVertex);
                        previousPoint.Add(adjacentVertex, p);
                    }
                }
            }
            List<Point> path = new List<Point>
            {
                endpoint
            };
            Point current = endpoint;
            while (previousPoint[current] != startpoint)
            {
                path.Add(previousPoint[current]);
                current = previousPoint[current];
            }
            path.Add(startpoint);
            path.Reverse();
            return path;
        }

        public bool ContainsPoint(Point point)
        {
            return adjacencyLists.ContainsKey(point);
        }
    }
}
