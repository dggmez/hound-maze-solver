﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HoundMazeSolver
{
    /// <summary>
    /// Simple two-dimensional immmutable point structure.
    /// </summary>
    struct Point
    {
        private int x;
        private int y;

        /// <summary>
        /// Gets the x-coordinate of the Point.
        /// </summary>
        public int X { get => x; }

        /// <summary>
        /// Gets the y-coordinate of the Point.
        /// </summary>
        public int Y { get => y; }

        /// <summary>
        /// Initializes an instance of the class with the specified coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Converts the Point to a readable string representation.
        /// </summary>
        /// <returns>String containing the representation.</returns>
        public override string ToString()
        {
            return "[" + X + ", " + Y + "]";
        }

        /// <summary>
        /// Compares two Point objects. The result specifies whether the two points have
        /// the same coordinates.
        /// </summary>
        /// <param name="p1">First operand.</param>
        /// <param name="p2">Second operand.</param>
        /// <returns>Boolean specifying wether the coordinates are the same.</returns>
        public static bool operator ==(Point p1, Point p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y;
        }

        /// <summary>
        /// Compares two Point objects. The result specifies whether the two points have
        /// different coordinates.
        /// </summary>
        /// <param name="p1">First operand.</param>
        /// <param name="p2">Second operand.</param>
        /// <returns>Boolean specifying whether the coordinates are different.</returns>
        public static bool operator !=(Point p1, Point p2)
        {
            return p1.X != p2.X || p1.Y != p2.Y;
        }

        /// <summary>
        /// Returns a boolean specifying if the Point has the same coordinates as the object.
        /// </summary>
        /// <param name="obj">Object to compare to.</param>
        /// <returns>Boolean specifying if the passed object has the same coordinates.</returns>
        public override bool Equals(object obj)
        {
            Point p2 = (Point)obj;
            return this == p2;
        }

        /// <summary>
        /// Returns a hash code for the Point.
        /// </summary>
        /// <returns>Hash code for the Point.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();
                return hash;
            }
        }
    }
}
