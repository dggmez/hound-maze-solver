﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HoundMazeSolver
{
    /// <summary>
    /// This is a very simple interactive console program to solve the challenge. It asks
    /// for the path to the TSV file representing the map, the startpoint and the endpoint
    /// (the last two in a [X, Y] format). As output it produces the list of points of the
    /// shortest path that should be followed and the map highlighting that path.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Enter path to the TSV file describing the map: ");
                var filePath = Console.ReadLine();
                MapParser parser;
                try
                {
                    using (var reader = File.OpenText(filePath))
                    {
                        parser = new MapParser(reader);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("ERROR: the path is invalid\n");
                    continue;
                }
                var graph = parser.Graph;
                Point startpoint;
                Point endpoint;
                while (true)
                {
                    Console.Write("Enter the coordinates of the startpoint in [X, Y] format: ");
                    var enteredStartpoint = Console.ReadLine();
                    try
                    {
                        startpoint = GetPoint(enteredStartpoint);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("ERROR: the entered point is not written in a valid format.\n");
                        continue;
                    }

                    if (!graph.ContainsPoint(startpoint))
                    {
                        Console.WriteLine("ERROR: the startpoint you entered is not valid point on the map.\n");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                while (true)
                {
                    Console.Write("Enter the coordinates of the endpoint in [X, Y] format: ");
                    var enteredEndpoint = Console.ReadLine();
                    try
                    {
                        endpoint = GetPoint(enteredEndpoint);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("ERROR: the entered point is not written in a valid format.\n");
                        continue;
                    }
                    if (!graph.ContainsPoint(endpoint))
                    {
                        Console.WriteLine("ERROR: the endpoint you entered is not a valid point on the map\n");
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                var path = graph.FindPath(startpoint, endpoint);
                Console.WriteLine("\nPath to follow:\n");
                for (int i = 0; i < path.Count - 1; i++)
                {
                    Console.Write("{0}, ", path[i]);
                }
                Console.WriteLine("{0}\n", path[path.Count - 1]);
                Console.WriteLine("Map:\n");
                WriteMap(graph, path);
                break;
            }
        }

        /// <summary>
        /// Writes to the console the map and the path that is to be followed from the
        /// startpoint to the point.
        /// </summary>
        /// <param name="graph">Graph representing the map.</param>
        /// <param name="path">The path that should be followed.</param>
        private static void WriteMap(Graph graph, List<Point> path)
        {
            int minYCoordinate = graph.Vertexes.OrderBy(p => p.Y).First().Y;
            int originTop = Console.CursorTop;
            int originLeft = Console.CursorLeft;
            int maxTop = 0;
            char character = 'F';
            ConsoleColor defaultColor = Console.ForegroundColor;
            foreach (var p in graph.Vertexes)
            {
                Console.SetCursorPosition(originLeft + p.X, originTop + p.Y - minYCoordinate);
                if (path.Contains(p))
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else
                {
                    Console.ForegroundColor = defaultColor;
                }
                if (p == path[0])
                {
                    character = 'S';
                }
                else if (p == path[path.Count - 1])
                {
                    character = 'E';
                }
                else
                {
                    character = 'F';
                }
                Console.Write(character);
                maxTop = maxTop < p.Y ? p.Y : maxTop;
            }
            Console.ForegroundColor = defaultColor;
            Console.SetCursorPosition(originLeft , originTop + maxTop - minYCoordinate + 2);
        }

        /// <summary>
        /// Converts a string representing a point in a [X, Y] format to an instance of a Point.
        /// </summary>
        /// <param name="enteredPoint">String representing the point.</param>
        /// <returns></returns>
        private static Point GetPoint(string enteredPoint)
        {
            if (string.IsNullOrWhiteSpace(enteredPoint))
            {
                throw new ArgumentException("The string is null or whitespace.", nameof(enteredPoint));
            }

            var coordinates = enteredPoint.Trim().Replace("[", "").Replace("]", "").Split(',');
            return new Point(int.Parse(coordinates[0]), int.Parse(coordinates[1]));
        }
    }
}
