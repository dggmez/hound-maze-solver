﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HoundMazeSolver
{
    /// <summary>
    /// Parses a TSV-formatted text containing the description of a map to generate
    /// a connected graph structure where each vertex is a walkable cell of the map.
    /// </summary>
    class MapParser
    {
        private Graph graph;

        /// <summary>
        /// Initializes a new instance of MapParser based on the specified TextReader.
        /// </summary>
        /// <param name="reader"></param>
        public MapParser(TextReader reader)
        {
            ParseStream(reader);
        }

        /// <summary>
        /// Provides the connected graph representation of the map.
        /// </summary>
        public Graph Graph
        {
            get
            {
                return graph;
            }
        }

        private void ParseStream(TextReader reader)
        {
            graph = new Graph();
            int x = 0;
            int y = 0;
            while (true)
            {
                int integer = reader.Read();
                if (integer == -1)
                {
                    break;
                }
                char character = (char)integer;
                if (character == '\t')
                {
                    x++;
                }
                else if (character == '\n')
                {
                    x = 0;
                    y++;
                }
                else if (character == 'F')
                {
                    graph.AddVertex(new Point(x, y));
                }
            }
        }
    }
}
